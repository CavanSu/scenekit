//
//  GameViewController.swift
//  GeometryFighter
//
//  Created by CavanSu on 2019/12/24.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var scnView: SCNView {
        return self.view as! SCNView
    }
    
    var scnScene: SCNScene!
    var cameraNode: SCNNode!
    
    var spawnTime: TimeInterval = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScene()
        setupSceneView()
        setupCamera()
        spawnShape()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let location = touch.location(in: scnView)
        let hitResults = scnView.hitTest(location, options: nil)
        
        if let result = hitResults.first {
          handleTouchFor(node: result.node)
        }
    }
}

extension GameViewController {
    func setupScene() {
        scnScene = SCNScene()
        scnView.scene = scnScene
        scnScene.background.contents = "art.scnassets/Textures/background.png"
    }
    
    func setupSceneView() {
        scnView.showsStatistics = true
        
        /*
         Single finger swipe - 单指滑动：围绕场景内容旋转活动摄像机。
         Two finger swipe - 双指滑动：在场景中向左，向右，向上或向下移动或平移相机。
         双指捏 - Two finger pinch：将相机放入和移出场景。
         双击 - Double-tap：如果您有多个摄像头，则会在场景中的摄像头之间切换。
                           当然，由于场景中只有一台摄像机，因此不会这样做。 但是，它还具有将相机重置为其原始位置和设置的效果。
         */
        
        scnView.allowsCameraControl = true
        
        // 在场景中创建一个通用的全向灯
        scnView.autoenablesDefaultLighting = true
        scnView.delegate = self
        
        // 默认情况下，如果没有要播放的动画，SceneKit会进入 “paused” 状态。要防止这种情况发生，必须在SCNView实例上启用playing属性
        scnView.isPlaying = true
    }
    
    func setupCamera() {
      cameraNode = SCNNode()
      cameraNode.camera = SCNCamera()
      cameraNode.position = SCNVector3(x: 0, y: 0, z: 10)
      scnScene.rootNode.addChildNode(cameraNode)
    }
    
    func spawnShape() {
        var geometry: SCNGeometry
        let shape = ShapeType.random()
        print("shape: \(shape)")
        
        switch shape {
        case .box:
            geometry = SCNBox(width: 1.0,
                              height: 1.0,
                              length: 1.0,
                              chamferRadius: 0.0)
        case .capsule:
            geometry = SCNCapsule(capRadius: 0.5, height: 1.0)
        case .cone:
            geometry = SCNCone(topRadius: 0.5, bottomRadius: 0.5, height: 1.0)
        default:
            geometry = SCNBox(width: 1.0,
                              height: 1.0,
                              length: 1.0,
                              chamferRadius: 0.0)
        }
        
        // 给几何体添加颜色
        geometry.materials.first?.diffuse.contents = UIColor.red
        
        let geometryNode = SCNNode(geometry: geometry)
        
        // 添加物理实体，使受物理场影响
        let body = SCNPhysicsBody(type: .dynamic,
                                  shape: nil)
        
        // 施加力
        let force = SCNVector3(1, 1, 0) // 力向量
        let position = SCNVector3(-0.5, -0.5, 0) // 力施加的位置
        
        geometryNode.physicsBody = body
        
        body.applyForce(force, at: position, asImpulse: true)
        
        // 关闭重力
        closeGravity(true)
        
        scnScene.rootNode.addChildNode(geometryNode)
    }
    
    func closeGravity(_ isColose: Bool) {
        // 关闭物理重力
        if isColose {
            scnScene.physicsWorld.gravity = SCNVector3(0, 0, 0)
        }
    }
    
    func cleanScene() {
        // 移除节点
        for node in scnScene.rootNode.childNodes where node.presentation.position.x > 5 {
            node.removeFromParentNode()
        }
    }
    
    func handleTouchFor(node: SCNNode) {
        node.removeFromParentNode()
    }
}

extension GameViewController: SCNSceneRendererDelegate {
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if time > spawnTime {
          spawnShape()
          spawnTime = time + TimeInterval(Float.random(in: 0.2..<1.5))
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        cleanScene()
    }
}

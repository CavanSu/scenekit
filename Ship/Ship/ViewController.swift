//
//  ViewController.swift
//  Ship
//
//  Created by CavanSu on 2019/12/23.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // cameraNode
        let camera = SCNCamera()
        camera.fieldOfView = 15
        
        let cameraNode = SCNNode()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(x: 0, y: 1, z: 10)
        scene.rootNode.addChildNode(cameraNode)
        
        let floor = SCNFloor()
        floor.materials.first?.diffuse.contents = UIColor.black
        let floorNode = SCNNode(geometry: floor)
        floorNode.position = SCNVector3(0, -10, 0)
        scene.rootNode.addChildNode(floorNode)
        
        let floorPhysicsBody = SCNPhysicsBody(type: .static,
                                              shape: SCNPhysicsShape(geometry: floor,
                                                                     options: nil))
        floorNode.physicsBody = floorPhysicsBody
        
        
        // Set the scene to the view
        sceneView.scene = scene
        sceneView.allowsCameraControl = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

//
//  ViewController.swift
//  Vans3D
//
//  Created by CavanSu on 2019/12/29.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var scnView: SCNView {
        return self.view as! SCNView
    }
    
    var scnScene: SCNScene!
    var cameraNode: SCNNode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSceneView()
        setupScene()
        setupShoesNode()
        setupCamera()
        defualtBlueColor()
    }
    
    func setupSceneView() {
        /*
         Single finger swipe - 单指滑动：围绕场景内容旋转活动摄像机。
         Two finger swipe - 双指滑动：在场景中向左，向右，向上或向下移动或平移相机。
         双指捏 - Two finger pinch：将相机放入和移出场景。
         双击 - Double-tap：如果您有多个摄像头，则会在场景中的摄像头之间切换。
         当然，由于场景中只有一台摄像机，因此不会这样做。 但是，它还具有将相机重置为其原始位置和设置的效果。
         */
        
        scnView.allowsCameraControl = true
        scnView.autoenablesDefaultLighting = true
    }
    
    func setupScene() {
        // 可以通过以下两种方式来加载 3d 文件，也可以使用 setupShoesNode
        /*
        let filePath = "art.scnassets/vans-authentic-shoe-low-poly.dae"
        guard let url = Bundle.main.url(forResource: filePath, withExtension: nil) else {
            return
        }

        let sceneSource = SCNSceneSource(url: url, options: nil)
        guard let scene = sceneSource?.scene(options: nil) else {
            return
        }

        scnScene = scene
        
        scnScene = SCNScene(named: "art.scnassets/vans-authentic-shoe-low-poly.dae")!
        */
        
        scnScene = SCNScene()
        scnView.scene = scnScene
        scnScene.background.contents = "art.scnassets/Textures/background.png"
    }
    
    func setupShoesNode() {
        let filePath = "art.scnassets/vans-authentic-shoe-low-poly.dae"
        guard let url = Bundle.main.url(forResource: filePath, withExtension: nil) else {
            return
        }
        
        guard let shoesNode = SCNReferenceNode(url: url) else {
            return
        }
        
        let scale = 0.3
        shoesNode.scale = SCNVector3(scale, scale, scale)
        shoesNode.position = SCNVector3(0, -5, 0)
        shoesNode.load()
        scnScene.rootNode.addChildNode(shoesNode)
    }
    
    func setupCamera() {
        let camera = SCNCamera()
        camera.zFar = 120.0
        camera.zNear = 0.0
        
        cameraNode = SCNNode()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 80)
        
        scnScene.rootNode.addChildNode(cameraNode)
    }
    
    @IBAction func doBlueColorPressed(_ sender: UIButton) {
        defualtBlueColor()
    }
    
    @IBAction func doWhiteColorPressed(_ sender: UIButton) {
        changeShoesSurfaceColor(UIColor.white)
    }
    
    @IBAction func doRedColorPressed(_ sender: UIButton) {
        changeShoesSurfaceColor(UIColor.red)
    }
}

private extension ViewController {
    func defualtBlueColor() {
        let blueColor = UIColor(red: 1.0 / 255.0, green: 16.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
        changeShoesSurfaceColor(blueColor)
    }
    
    func changeShoesSurfaceColor(_ color: UIColor) {
        let surfaceLeftNode = scnView.scene?.rootNode.childNode(withName: "polySurface394", recursively: true)
        surfaceLeftNode?.geometry?.materials.first?.diffuse.contents = color
        
        let surfaceRightNode = scnView.scene?.rootNode.childNode(withName: "_polySurface207", recursively: true)
        surfaceRightNode?.geometry?.materials.first?.diffuse.contents = color
        
        print("camera.position: \(cameraNode.position)")
        print("camera.zFar: \(cameraNode.camera!.zFar)")
        print("camera.zNear: \(cameraNode.camera!.zNear)")
        print("camera.xFov: \(cameraNode.camera!.xFov)")
        print("camera.yFov: \(cameraNode.camera!.yFov)")
        
        print("camera.fieldOfView: \(cameraNode.camera!.fieldOfView)")
        print("camera.focalLength: \(cameraNode.camera!.focalLength)")
    }
}
